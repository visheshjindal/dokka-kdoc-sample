import org.gradle.kotlin.dsl.kotlin
import org.gradle.plugin.use.PluginDependenciesSpec
import org.gradle.plugin.use.PluginDependencySpec

fun PluginDependenciesSpec.dokka(): PluginDependencySpec =
    id("org.jetbrains.dokka").version("1.9.10")